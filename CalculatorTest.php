<?php

require 'Calculator.php';

class CalculatorTest extends PHPUnit_Framework_TestCase
{
    
    private $calculator;
    
    public function setUp()
    {
        $this->calculator = new Calculator();
    }
    
    public function testIfAddOk()
    {       
        $add = $this->calculator->add(1, 2);        
        $this->assertEquals(3,$add);//OK           
    }
    
    public function testIfAddNotKo()
    {      
        $add = $this->calculator->add(1, 2);
        
        $this->assertNotEquals(4,$add);// Not Failed
    }
    
    public function testGeneralizationAddOk()
    {       
        $datas = [
                    [10,5,5],
                    [23,10,13],
                    [25,5,20]
                 ];
        foreach($datas as $data){
            $add = $this->calculator->add($data[1],$data[2]);
            $this->assertEquals($data[0],$add);//OK
        }
    }
    
    public function testValidityArgumentOK()
    {
        $add = $this->calculator->add(2,1);
        $this->assertNotEquals(4,$add);//Failed
    }
    
}
