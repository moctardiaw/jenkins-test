<?php

class Calculator {
    /**
     * 
     * @param int $a
     * @param int $b
     * @return int
     */
    public function add($a,$b){
        if(!is_int($a)||!is_int($b))throw new InvalidArgumentException;     
        return $a+$b;
    }
    /**
     * 
     * @param int $a
     * @param int $b
     * @return int
     */
    public function sub($a,$b){
        return $a-$b;
    }
    
    /**
     * 
     * @param int $a
     * @param int $b
     * @return int
     */
    public function multi($a,$b){
        return $a*$b;
    }
    /**
     * 
     * @param int $a
     * @param int $b
     * @return int
     * @throws Exception
     */
    public function div($a,$b){
        if($b==0)throw new Exception("0 is not allow");
        else return $a/$b;
    }
}

